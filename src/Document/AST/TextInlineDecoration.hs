{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE LambdaCase #-}

module Document.AST.TextInlineDecoration(
  TextInlineDecoration(..)
, HasTextInlineDecoration(..)
, AsTextInlineDecoration(..)
, ManyTextInlineDecoration(..)
) where

import Control.Category((.), id)
import Control.Lens(Prism', Lens', Traversal', prism')
import Data.Eq(Eq)
import Data.Maybe(Maybe(Nothing, Just))
import Data.Ord(Ord)
import GHC.Show(Show)

data TextInlineDecoration =
  Emphasis
  | Strong
  | Underline
  | Overline
  | Strikeout
  | Subscript
  | Superscript
  deriving (Eq, Ord, Show)

class AsTextInlineDecoration a where
  _TextInlineDecoration ::
    Prism' a TextInlineDecoration
  _Emphasis ::
    Prism' a ()
  _Emphasis =
    _TextInlineDecoration . _Emphasis
  _Strong ::
    Prism' a ()
  _Strong =
    _TextInlineDecoration . _Strong
  _Underline ::
    Prism' a ()
  _Underline =
    _TextInlineDecoration . _Underline
  _Overline ::
    Prism' a ()
  _Overline =
    _TextInlineDecoration . _Overline
  _Strikeout ::
    Prism' a ()
  _Strikeout =
    _TextInlineDecoration . _Strikeout
  _Subscript ::
    Prism' a ()
  _Subscript =
    _TextInlineDecoration . _Subscript
  _Superscript ::
    Prism' a ()
  _Superscript =
    _TextInlineDecoration . _Superscript

instance AsTextInlineDecoration TextInlineDecoration where
  _TextInlineDecoration =
    id
  _Emphasis =
    prism'
      (\() -> Emphasis)
      (
        \case
          Emphasis ->
            Just ()
          _ ->
            Nothing
      )
  _Strong =
    prism'
      (\() -> Strong)
      (
        \case
          Strong ->
            Just ()
          _ ->
            Nothing
      )
  _Underline =
    prism'
      (\() -> Underline)
      (
        \case
          Underline ->
            Just ()
          _ ->
            Nothing
      )
  _Overline =
    prism'
      (\() -> Overline)
      (
        \case
          Overline ->
            Just ()
          _ ->
            Nothing
      )
  _Strikeout =
    prism'
      (\() -> Strikeout)
      (
        \case
          Strikeout ->
            Just ()
          _ ->
            Nothing
      )
  _Subscript =
    prism'
      (\() -> Subscript)
      (
        \case
          Subscript ->
            Just ()
          _ ->
            Nothing
      )
  _Superscript =
    prism'
      (\() -> Superscript)
      (
        \case
          Superscript ->
            Just ()
          _ ->
            Nothing
      )

class HasTextInlineDecoration a where
  textInlineDecoration ::
    Lens' a TextInlineDecoration

instance HasTextInlineDecoration TextInlineDecoration where
  textInlineDecoration =
    id

class ManyTextInlineDecoration a where
  _TextInlineDecoration_ ::
    Traversal' a TextInlineDecoration

instance ManyTextInlineDecoration TextInlineDecoration where
  _TextInlineDecoration_ =
    id
