{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}

module Document.AST.ParagraphSegment(
  ParagraphSegment(..)
, AsParagraphSegment(..)
, HasParagraphSegment(..)
, ManyParagraphSegment(..)
, txt
) where

import Control.Category((.), id)
import Control.Lens(Prism', Lens', Traversal', prism')
import Data.Eq(Eq)
import Data.Functor(Functor(fmap), (<$>))
import Data.Maybe(Maybe(Nothing, Just))
import Document.AST.Link(Link(Link), AsLink(_Link))
import Document.AST.LinkStyle(LinkStyle)
import Document.AST.TextInline(TextInline, HasTextInline(textInline), AsTextInline(_TextInline), ManyTextInline(_TextInline_), textInline, textInline0)
import Document.AST.TextInlineDecoration(ManyTextInlineDecoration(_TextInlineDecoration_))
import Document.AST.TextInlineDecorations(ManyTextInlineDecorations(_TextInlineDecorations_), textInlineDecorations)
import GHC.Show(Show)

data ParagraphSegment s =
  TextInlineParagraphSegment (TextInline s)
  | LinkParagraphSegment (Link s)
  deriving (Eq, Show)

instance Functor ParagraphSegment where
  fmap f (TextInlineParagraphSegment x) =
    TextInlineParagraphSegment (fmap f x)
  fmap f (LinkParagraphSegment x) =
    LinkParagraphSegment (fmap f x)
    
class AsParagraphSegment s t | s -> t where
  _ParagraphSegment ::
    Prism' s (ParagraphSegment t)

instance AsParagraphSegment (ParagraphSegment t) t where
  _ParagraphSegment =
    id

class HasParagraphSegment s t | s -> t where
  paragraphSegment ::
    Lens' s (ParagraphSegment t)
  linkHrefStyle ::
    Lens' s (Maybe (t, LinkStyle t))
  linkHrefStyle =
    paragraphSegment . 
      \f ->
        \case
          TextInlineParagraphSegment x ->
            (
              \case
                Nothing ->
                  TextInlineParagraphSegment x
                Just (c', s') ->
                  LinkParagraphSegment (Link c' x s')
            ) <$> f Nothing
          LinkParagraphSegment (Link c x s) ->
            (
              \case
                Nothing ->
                  TextInlineParagraphSegment x
                Just (c', s') ->
                  LinkParagraphSegment (Link c' x s')
            ) <$> f (Just (c, s))
      
instance HasTextInline (ParagraphSegment t) t where
  textInline f =
    \case
      TextInlineParagraphSegment x ->
        TextInlineParagraphSegment <$> f x
      LinkParagraphSegment x ->
        LinkParagraphSegment <$> textInline f x

instance HasParagraphSegment (ParagraphSegment t) t where
  paragraphSegment =
    id

class ManyParagraphSegment s t | s -> t where
  _ParagraphSegment_ ::
    Traversal' s (ParagraphSegment t)

instance ManyParagraphSegment (ParagraphSegment t) t where
  _ParagraphSegment_ =
    id

instance AsTextInline (ParagraphSegment s) s where
  _TextInline =
    prism'
      (\x -> TextInlineParagraphSegment x)
      (
        \case
          TextInlineParagraphSegment x ->
            Just x
          LinkParagraphSegment _ ->
            Nothing
      )

instance AsLink (ParagraphSegment s) s where
  _Link =
    prism'
      (\x -> LinkParagraphSegment x)
      (
        \case
          LinkParagraphSegment x ->
            Just x
          TextInlineParagraphSegment  _ ->
            Nothing
      )

instance ManyTextInline (ParagraphSegment s) s where
  _TextInline_ =
    textInline

instance ManyTextInlineDecorations (ParagraphSegment s) where
  _TextInlineDecorations_ f (TextInlineParagraphSegment x) =
    TextInlineParagraphSegment <$> textInlineDecorations f x
  _TextInlineDecorations_ f (LinkParagraphSegment x) =
    LinkParagraphSegment <$> textInlineDecorations f x

instance ManyTextInlineDecoration (ParagraphSegment s) where
  _TextInlineDecoration_ =
    _TextInlineDecorations_ . _TextInlineDecoration_

txt ::
  s
  -> ParagraphSegment s
txt =
  TextInlineParagraphSegment . textInline0
