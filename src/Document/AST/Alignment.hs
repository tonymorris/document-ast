{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE LambdaCase #-}

module Document.AST.Alignment(
  Alignment(..)
, HasAlignment(..)
, AsAlignment(..)
, ManyAlignment(..)
) where

import Control.Category((.), id)
import Control.Lens(Lens', Traversal', Prism', prism')
import Data.Eq(Eq)
import Data.Maybe(Maybe(Just, Nothing))
import Data.Ord(Ord)
import GHC.Show(Show)

data Alignment =
  Left
  | Centre
  | Right
  deriving (Eq, Ord, Show)

class HasAlignment a where
  blockAlignment ::
    Lens' a Alignment

instance HasAlignment Alignment where
  blockAlignment =
    id

class AsAlignment a where
  _Alignment ::
    Prism' a Alignment
  _Left ::
    Prism' a ()
  _Right ::
    Prism' a ()
  _Centre ::
    Prism' a ()
  _Left = 
    _Alignment . _Left
  _Right = 
    _Alignment . _Right
  _Centre = 
    _Alignment . _Centre

instance AsAlignment Alignment where
  _Alignment =
    id
  _Left = 
    prism'
      (\() -> Left)
      (
        \case
          Left ->
            Just ()
          _ ->
            Nothing
      )
  _Right = 
    prism'
      (\() -> Right)
      (
        \case
          Right ->
            Just ()
          _ ->
            Nothing
      )
  _Centre = 
    prism'
      (\() -> Centre)
      (
        \case
          Centre ->
            Just ()
          _ ->
            Nothing
      )

class ManyAlignment a where
  _Alignment_ ::
    Traversal' a Alignment

instance ManyAlignment Alignment where
  _Alignment_ =
    id
