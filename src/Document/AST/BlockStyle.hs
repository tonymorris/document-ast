{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}

module Document.AST.BlockStyle(
  BlockStyle(..)
, HasBlockStyle(..)
, AsBlockStyle(..)
, ManyBlockStyle(..)
, blockStyleForegroundColour'
, blockStyleBackgroundColour'
, blockStyleFont'
, blockStyleFontSize'
, blockStyleAlignment'
) where

import Control.Applicative((<|>))
import Control.Category((.), id)
import Control.Lens(Lens', Prism', Traversal', _Just)
import Data.Colour(Colour)
import Data.Eq(Eq)
import Data.Functor(Functor(fmap))
import Data.Maybe(Maybe(Nothing))
import Data.Semigroup(Semigroup((<>)))
import Data.Monoid(Monoid(mappend, mempty))
import Document.AST.Alignment(Alignment, ManyAlignment(_Alignment_))
import Document.AST.FontSize(FontSize, ManyFontSize(_FontSize_))
import GHC.Float(Float)
import GHC.Show(Show)

data BlockStyle s =
  BlockStyle
    (Maybe (Colour Float)) -- foreground
    (Maybe (Colour Float)) -- background
    (Maybe s) -- font
    (Maybe FontSize)
    (Maybe Alignment)
  deriving (Eq, Show)

instance Semigroup (BlockStyle s) where
  BlockStyle c1 b1 t1 s1 a1 <> BlockStyle c2 b2 t2 s2 a2 =
    BlockStyle (c1 <|> c2) (b1 <|> b2) (t1 <|> t2) (s1 <|> s2) (a1 <|> a2)
    
instance Monoid (BlockStyle s) where
  mappend =
    (<>)
  mempty =
      BlockStyle
        Nothing
        Nothing
        Nothing
        Nothing
        Nothing

instance Functor BlockStyle where
  fmap f (BlockStyle c b t s a) =
    BlockStyle c b (fmap f t) s a

class HasBlockStyle a t | a -> t where
  blockStyle ::
    Lens' a (BlockStyle t)
  blockStyleForegroundColour ::
    Lens' a (Maybe (Colour Float))
  blockStyleBackgroundColour ::
    Lens' a (Maybe (Colour Float))
  blockStyleFont ::
    Lens' a (Maybe t)
  blockStyleFontSize ::
    Lens' a (Maybe FontSize)
  blockStyleAlignment ::
    Lens' a (Maybe Alignment)
  blockStyleForegroundColour =
    blockStyle . blockStyleForegroundColour
  blockStyleBackgroundColour =
    blockStyle . blockStyleBackgroundColour
  blockStyleFont =
    blockStyle . blockStyleFont
  blockStyleFontSize =
    blockStyle . blockStyleFontSize
  blockStyleAlignment =
    blockStyle . blockStyleAlignment

instance HasBlockStyle (BlockStyle s) s where
  blockStyle =
    id
  blockStyleForegroundColour f (BlockStyle c b t s a) =
    fmap (\c' -> BlockStyle c' b t s a) (f c)
  blockStyleBackgroundColour f (BlockStyle c b t s a) =
    fmap (\b' -> BlockStyle c b' t s a) (f b)
  blockStyleFont f (BlockStyle c b t s a) =
    fmap (\t' -> BlockStyle c b t' s a) (f t)
  blockStyleFontSize f (BlockStyle c b t s a) =
    fmap (\s' -> BlockStyle c b t s' a) (f s)
  blockStyleAlignment f (BlockStyle c b t s a) =
    fmap (\a' -> BlockStyle c b t s a') (f a)

blockStyleForegroundColour' ::
  HasBlockStyle a t =>
  Traversal' a (Colour Float)
blockStyleForegroundColour' =
  blockStyleForegroundColour . _Just

blockStyleBackgroundColour' ::
  HasBlockStyle a t =>
  Traversal' a (Colour Float)
blockStyleBackgroundColour' =
  blockStyleBackgroundColour . _Just

blockStyleFont' ::
  HasBlockStyle a t =>
  Traversal' a t
blockStyleFont' =
  blockStyleFont . _Just

blockStyleFontSize' ::
  HasBlockStyle a t =>
  Traversal' a FontSize
blockStyleFontSize' =
  blockStyleFontSize . _Just

blockStyleAlignment' ::
  HasBlockStyle a t =>
  Traversal' a Alignment
blockStyleAlignment' =
  blockStyleAlignment . _Just

class AsBlockStyle a t | a -> t where
  _BlockStyle ::
    Prism' a (BlockStyle t)

instance AsBlockStyle (BlockStyle s) s where
  _BlockStyle =
    id

class ManyBlockStyle a t | a -> t where
  _BlockStyle_ ::
    Traversal' a (BlockStyle t)

instance ManyBlockStyle (BlockStyle s) s where
  _BlockStyle_ =
    id

instance ManyFontSize (BlockStyle s) where
  _FontSize_ =
    blockStyleFontSize . _Just
    
instance ManyAlignment (BlockStyle s) where
  _Alignment_ =
    blockStyleAlignment . _Just
