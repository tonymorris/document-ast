{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}

module Document.AST.StyledParagraphSegment(
  StyledParagraphSegment(..)
, HasStyledParagraphSegment(..)
, AsStyledParagraphSegment(..)
, ManyStyledParagraphSegment(..)
) where

import Control.Category(id)
import Control.Lens(Lens', Prism', Traversal')
import Data.Eq(Eq)
import Data.Functor(Functor(fmap))
import Document.AST.ParagraphSegment(ParagraphSegment, HasParagraphSegment(paragraphSegment), ManyParagraphSegment(_ParagraphSegment_))
import Document.AST.ParagraphSegmentStyle(ParagraphSegmentStyle, HasParagraphSegmentStyle(paragraphSegmentStyle), ManyParagraphSegmentStyle(_ParagraphSegmentStyle_))
import GHC.Show(Show)

data StyledParagraphSegment s =
  StyledParagraphSegment
    (ParagraphSegment s)
    (ParagraphSegmentStyle s)
  deriving (Eq, Show)

instance Functor StyledParagraphSegment where
  fmap f (StyledParagraphSegment b s) =
    StyledParagraphSegment (fmap f b) (fmap f s)

class HasStyledParagraphSegment s t | s -> t where
  styledParagraphSegment ::
    Lens' s (StyledParagraphSegment t)
    
instance HasStyledParagraphSegment (StyledParagraphSegment s) s where
  styledParagraphSegment =
    id

class AsStyledParagraphSegment s t | s -> t where
  _StyledParagraphSegment ::
    Prism' s (StyledParagraphSegment t)

instance AsStyledParagraphSegment (StyledParagraphSegment s) s where
  _StyledParagraphSegment =
    id
    
class ManyStyledParagraphSegment s t | s -> t where
  _StyledParagraphSegment_ ::
    Traversal' s (StyledParagraphSegment t)
    
instance ManyStyledParagraphSegment (StyledParagraphSegment s) s where
  _StyledParagraphSegment_ =
    id

instance HasParagraphSegment (StyledParagraphSegment s) s where
  paragraphSegment f (StyledParagraphSegment p s) =
    fmap (\p' -> StyledParagraphSegment p' s) (f p)

instance ManyParagraphSegment (StyledParagraphSegment s) s where
  _ParagraphSegment_ =
    paragraphSegment

instance HasParagraphSegmentStyle (StyledParagraphSegment s) s where
  paragraphSegmentStyle f (StyledParagraphSegment p s) =
    fmap (\s' -> StyledParagraphSegment p s') (f s)

instance ManyParagraphSegmentStyle (StyledParagraphSegment s) s where
  _ParagraphSegmentStyle_ =
    paragraphSegmentStyle
