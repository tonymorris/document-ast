{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}

module Document.AST.TextInline(
  TextInline(..)
, HasTextInline(..)
, AsTextInline(..)
, ManyTextInline(..)
, textInline0
) where

import Control.Applicative(Applicative(pure, (<*>)))
import Control.Category((.), id)
import Control.Lens(Traversal', Prism', Lens')
import Control.Monad(Monad(return, (>>=)))
import Data.Eq(Eq)
import Data.Functor(Functor(fmap))
import Data.Monoid(Monoid(mappend, mempty))
import Data.Ord(Ord)
import Data.Semigroup(Semigroup((<>)))
import Document.AST.TextInlineDecorations(TextInlineDecorations, HasTextInlineDecorations(textInlineDecorations))
import GHC.Show(Show)

data TextInline s =
  TextInline s TextInlineDecorations
  deriving (Eq, Ord, Show)

instance Semigroup s => Semigroup (TextInline s) where
  TextInline i d <> TextInline j e =
    TextInline (i <> j) (d <> e)

instance Monoid s => Monoid (TextInline s) where
  TextInline i d `mappend` TextInline j e =
    TextInline (i `mappend` j) (d `mappend` e)
  mempty =
    TextInline mempty mempty
    
instance Functor TextInline where
  fmap f (TextInline i d) =
    TextInline (f i) d

instance Applicative TextInline where
  pure a =
    TextInline a mempty
  TextInline f d <*> TextInline a e =
    TextInline (f a) (d <> e)

instance Monad TextInline where
  return =
    pure
  TextInline a d >>= f =
    let TextInline b e =
          f a
    in  TextInline b (d <> e)

class HasTextInline s t | s -> t where
  textInline ::
    Lens' s (TextInline t)
  inlineText ::
    Lens' s t
  inlineText =
    textInline . inlineText
  {-# INLINE inlineText #-}

instance HasTextInline (TextInline t) t where
  textInline =
    id
  {-# INLINE inlineText #-}
  inlineText f (TextInline i d) =
    fmap (\i' -> TextInline i' d) (f i)

class AsTextInline s t | s -> t where
  _TextInline ::
    Prism' s (TextInline t)

instance AsTextInline (TextInline t) t where
  _TextInline =
    id

class ManyTextInline s t | s -> t where
  _TextInline_ ::
    Traversal' s (TextInline t)

instance ManyTextInline (TextInline t) t where
  _TextInline_ =
    id

instance HasTextInlineDecorations (TextInline s) where
  textInlineDecorations f (TextInline s x) =
    fmap (\x' -> TextInline s x') (f x)

textInline0 ::
  s
  -> TextInline s
textInline0 s =
  TextInline
    s
    mempty
