{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}

module Document.AST.Link(
  Link(..)
, HasLink(..)
, AsLink(..)
, ManyLink(..)
) where

import Control.Category((.), id)
import Control.Lens(Traversal', Prism', Lens')
import Data.Eq(Eq)
import Data.Functor(Functor(fmap))
import Data.Monoid(Monoid(mempty, mappend))
import Data.Semigroup(Semigroup((<>)))
import Document.AST.LinkStyle(LinkStyle, HasLinkStyle(linkStyle))
import Document.AST.TextInline(TextInline, HasTextInline(textInline))
import Document.AST.TextInlineDecorations(HasTextInlineDecorations(textInlineDecorations))
import GHC.Show(Show)

data Link s =
  Link
    s
    (TextInline s)
    (LinkStyle s)
  deriving (Eq, Show)

instance Semigroup s => Semigroup (Link s) where
  Link a x s <> Link b y t =
    Link (a <> b) (x <> y) (s <> t)

instance Monoid s => Monoid (Link s) where
  Link a x s `mappend` Link b y t =
    Link (a `mappend` b) (x `mappend` y) (s `mappend` t)
  mempty =
    Link mempty mempty mempty
    
instance Functor Link where
  fmap f (Link a x s) =
    Link (f a) (fmap f x) (fmap f s)

class HasLink s t | s -> t where
  link ::
    Lens' s (Link t)
  linkto ::
    Lens' s t
  linkto =
    link . linkto
  {-# INLINE linkto #-}

instance HasLink (Link t) t where
  link =
    id
  linkto f (Link t x s) =
    fmap (\t' -> Link t' x s) (f t)
  {-# INLINE linkto #-}

class AsLink s t | s -> t where
  _Link ::
    Prism' s (Link t)

instance AsLink (Link t) t where
  _Link =
    id

class ManyLink s t | s -> t where
  _Link_ ::
    Traversal' s (Link t)

instance ManyLink (Link t) t where
  _Link_ =
    id

instance HasTextInline (Link s) s where
  textInline f (Link t x s) =
    fmap (\x' -> Link t x' s) (f x)

instance HasLinkStyle (Link s) s where
  linkStyle f (Link t x s) =
    fmap (\s' -> Link t x s') (f s)

instance HasTextInlineDecorations (Link s) where
  textInlineDecorations =
    textInline . textInlineDecorations
