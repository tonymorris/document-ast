{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE LambdaCase #-}

module Document.AST.HorizontalRule(
  HorizontalRule(..)
, HasHorizontalRule(..)
, AsHorizontalRule(..)
, ManyHorizontalRule(..)
) where

import Control.Category((.), id)
import Control.Lens(Lens', Prism', Traversal', _Just)
import Natural(Natural)
import Data.Colour(Colour)
import Data.Eq(Eq)
import Data.Functor(fmap)
import Data.Maybe(Maybe)
import Document.AST.Alignment
import Document.AST.HorizontalRuleStyle(HorizontalRuleStyle, ManyHorizontalRuleStyle(_HorizontalRuleStyle_))
import GHC.Float(Float)
import GHC.Show(Show)

-- <hr style="width: 50%; border: 90px dashed green; border-radius: 5px;transform: rotate(10deg);">
data HorizontalRule =
  HorizontalRule
    (Maybe HorizontalRuleStyle)
    (Maybe (Colour Float))
    (Maybe Alignment)
    (Maybe Float) -- width %
    (Maybe Natural) -- thickness px
    (Maybe Natural) -- radius px
  deriving (Eq, Show)

class HasHorizontalRule a where
  horizontalRule ::
    Lens' a HorizontalRule
  hrColour ::
    Lens' a (Maybe (Colour Float))
  hrStyle ::
    Lens' a (Maybe HorizontalRuleStyle)
  hrAlignment ::
    Lens' a (Maybe Alignment)
  hrWidth ::
    Lens' a (Maybe Float)
  hrThickness ::
    Lens' a (Maybe Natural)
  hrRadius ::
    Lens' a (Maybe Natural)
  hrStyle = 
    horizontalRule . hrStyle
  hrColour = 
    horizontalRule . hrColour
  hrWidth = 
    horizontalRule . hrWidth
  hrThickness = 
    horizontalRule . hrThickness
  hrRadius = 
    horizontalRule . hrRadius

instance HasHorizontalRule HorizontalRule where
  horizontalRule =
    id
  hrStyle f (HorizontalRule s c a w t r) =
    fmap (\s' -> HorizontalRule s' c a w t r) (f s)
  hrColour f (HorizontalRule s c a w t r) =
    fmap (\c' -> HorizontalRule s c' a w t r) (f c)
  hrAlignment f (HorizontalRule s c a w t r) =
    fmap (\a' -> HorizontalRule s c a' w t r) (f a)
  hrWidth f (HorizontalRule s c a w t r) =
    fmap (\w' -> HorizontalRule s c a w' t r) (f w)
  hrThickness f (HorizontalRule s c a w t r) =
    fmap (\t' -> HorizontalRule s c a w t' r) (f t)
  hrRadius f (HorizontalRule s c a w t r) =
    fmap (\r' -> HorizontalRule s c a w t r') (f r)

class AsHorizontalRule a where
  _HorizontalRule ::
    Prism' a HorizontalRule

instance AsHorizontalRule HorizontalRule where
  _HorizontalRule =
    id

class ManyHorizontalRule a where
  _HorizontalRule_ ::
    Traversal' a HorizontalRule

instance ManyHorizontalRule HorizontalRule where
  _HorizontalRule_ =
    id

instance ManyHorizontalRuleStyle HorizontalRule where
  _HorizontalRuleStyle_ =
    hrStyle . _Just
