{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}

module Document.AST.Heading(
  Heading(..)
, HasHeading(..)
, AsHeading(..)
, ManyHeading(..)
, h1
, h2
, h3
, h4
, h5
, h6
) where

import Control.Category((.), id)
import Control.Lens(Lens', Prism', Traversal', ( # ))
import Data.Eq(Eq)
import Data.Functor(Functor(fmap))
import Data.Ord(Ord)
import Document.AST.HeadingLevel(HeadingLevel(HeadingLevel1, HeadingLevel2, HeadingLevel3, HeadingLevel4, HeadingLevel5, HeadingLevel6), HasHeadingLevel(headingLevel))
import GHC.Show(Show)

data Heading s = 
  Heading HeadingLevel s
  deriving (Eq, Ord, Show)

instance Functor Heading where
  fmap f (Heading l s) =
    Heading l (f s)
    
class HasHeading s t | s -> t where
  heading ::
    Lens' s (Heading t)
  headingText ::
    Lens' s t
  headingText =
    heading . headingText
  {-# INLINE headingText #-}

instance HasHeading (Heading t) t where
  heading =
    id
  headingText f (Heading l t) =
    fmap (\t' -> Heading l t') (f t)
  {-# INLINE headingText #-}

class AsHeading s t | s -> t where
  _Heading ::
    Prism' s (Heading t)

instance AsHeading (Heading t) t where
  _Heading =
    id

class ManyHeading s t | s -> t where
  _Heading_ ::
    Traversal' s (Heading t)

instance ManyHeading (Heading t) t where
  _Heading_ =
    id

instance HasHeadingLevel (Heading s) where
  headingLevel f (Heading l t) =
    fmap (\l' -> Heading l' t) (f l)

h1 ::
  AsHeading x s =>
  s
  -> x
h1 x =
  _Heading # Heading HeadingLevel1 x

h2 ::
  AsHeading x s =>
  s
  -> x
h2 x =
  _Heading # Heading HeadingLevel2 x

h3 ::
  AsHeading x s =>
  s
  -> x
h3 x =
  _Heading # Heading HeadingLevel3 x

h4 ::
  AsHeading x s =>
  s
  -> x
h4 x =
  _Heading # Heading HeadingLevel4 x

h5 ::
  AsHeading x s =>
  s
  -> x
h5 x =
  _Heading # Heading HeadingLevel5 x

h6 ::
  AsHeading x s =>
  s
  -> x
h6 x =
  _Heading # Heading HeadingLevel6 x
