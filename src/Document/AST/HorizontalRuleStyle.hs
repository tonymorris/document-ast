{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE LambdaCase #-}

module Document.AST.HorizontalRuleStyle(
  HorizontalRuleStyle(..)
, HasHorizontalRuleStyle(..)
, AsHorizontalRuleStyle(..)
, ManyHorizontalRuleStyle(..)
) where

import Control.Category((.), id)
import Control.Lens(Lens', Prism', Traversal', prism')
import Data.Eq(Eq)
import Data.Maybe(Maybe(Just, Nothing))
import Data.Ord(Ord)
import GHC.Show(Show)

data HorizontalRuleStyle =
  Dotted
  | Dashed
  | Solid
  deriving (Eq, Ord, Show)

class HasHorizontalRuleStyle a where
  horizontalRuleStyle ::
    Lens' a HorizontalRuleStyle

instance HasHorizontalRuleStyle HorizontalRuleStyle where
  horizontalRuleStyle =
    id

class AsHorizontalRuleStyle a where
  _HorizontalRuleStyle ::
    Prism' a HorizontalRuleStyle
  _Dotted ::
    Prism' a ()
  _Dashed ::
    Prism' a ()
  _Solid ::
    Prism' a ()
  _Dotted =
    _HorizontalRuleStyle . _Dotted
  _Dashed =
    _HorizontalRuleStyle . _Dashed
  _Solid =
    _HorizontalRuleStyle . _Solid

instance AsHorizontalRuleStyle HorizontalRuleStyle where
  _HorizontalRuleStyle =
    id
  _Dotted =
    prism'
      (\() -> Dotted)
      (
        \case
          Dotted ->
            Just ()
          _ ->
            Nothing
      )
  _Dashed =
    prism'
      (\() -> Dashed)
      (
        \case
          Dashed ->
            Just ()
          _ ->
            Nothing
      )
  _Solid =
    prism'
      (\() -> Solid)
      (
        \case
          Solid ->
            Just ()
          _ ->
            Nothing
      )

class ManyHorizontalRuleStyle a where
  _HorizontalRuleStyle_ ::
    Traversal' a HorizontalRuleStyle

instance ManyHorizontalRuleStyle HorizontalRuleStyle where
  _HorizontalRuleStyle_ =
    id
