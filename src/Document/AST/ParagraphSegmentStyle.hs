{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}

module Document.AST.ParagraphSegmentStyle(
  ParagraphSegmentStyle(..)
, HasParagraphSegmentStyle(..)
, AsParagraphSegmentStyle(..)
, ManyParagraphSegmentStyle(..)
, paragraphSegmentForegroundColour'
, paragraphSegmentBackgroundColour'
, paragraphSegmentFont'
, paragraphSegmentFontSize'
) where

import Control.Applicative((<|>))
import Control.Category((.), id)
import Control.Lens(Lens', Prism', Traversal', _Just)
import Data.Colour(Colour)
import Data.Eq(Eq)
import Data.Functor(Functor(fmap))
import Data.Maybe(Maybe(Nothing))
import Data.Semigroup(Semigroup((<>)))
import Data.Monoid(Monoid(mappend, mempty))
import Document.AST.FontSize(FontSize, ManyFontSize(_FontSize_))
import GHC.Float(Float)
import GHC.Show(Show)

data ParagraphSegmentStyle s =
  ParagraphSegmentStyle
    (Maybe (Colour Float)) -- foreground
    (Maybe (Colour Float)) -- background
    (Maybe s) -- font
    (Maybe FontSize)
  deriving (Eq, Show)

instance Semigroup (ParagraphSegmentStyle s) where
  ParagraphSegmentStyle c1 b1 t1 s1 <> ParagraphSegmentStyle c2 b2 t2 s2 =
    ParagraphSegmentStyle (c1 <|> c2) (b1 <|> b2) (t1 <|> t2) (s1 <|> s2)
    
instance Monoid (ParagraphSegmentStyle s) where
  mappend =
    (<>)
  mempty =
      ParagraphSegmentStyle
        Nothing
        Nothing
        Nothing
        Nothing

instance Functor ParagraphSegmentStyle where
  fmap f (ParagraphSegmentStyle c b t s) =
    ParagraphSegmentStyle c b (fmap f t) s

class HasParagraphSegmentStyle a t | a -> t where
  paragraphSegmentStyle ::
    Lens' a (ParagraphSegmentStyle t)
  paragraphSegmentForegroundColour ::
    Lens' a (Maybe (Colour Float))
  paragraphSegmentBackgroundColour ::
    Lens' a (Maybe (Colour Float))
  paragraphSegmentFont ::
    Lens' a (Maybe t)
  paragraphSegmentFontSize ::
    Lens' a (Maybe FontSize)
  paragraphSegmentForegroundColour =
    paragraphSegmentStyle . paragraphSegmentForegroundColour
  paragraphSegmentBackgroundColour =
    paragraphSegmentStyle . paragraphSegmentBackgroundColour
  paragraphSegmentFont =
    paragraphSegmentStyle . paragraphSegmentFont
  paragraphSegmentFontSize =
    paragraphSegmentStyle . paragraphSegmentFontSize

instance HasParagraphSegmentStyle (ParagraphSegmentStyle s) s where
  paragraphSegmentStyle =
    id
  paragraphSegmentForegroundColour f (ParagraphSegmentStyle c b t s) =
    fmap (\c' -> ParagraphSegmentStyle c' b t s) (f c)
  paragraphSegmentBackgroundColour f (ParagraphSegmentStyle c b t s) =
    fmap (\b' -> ParagraphSegmentStyle c b' t s) (f b)
  paragraphSegmentFont f (ParagraphSegmentStyle c b t s) =
    fmap (\t' -> ParagraphSegmentStyle c b t' s) (f t)
  paragraphSegmentFontSize f (ParagraphSegmentStyle c b t s) =
    fmap (\s' -> ParagraphSegmentStyle c b t s') (f s)

paragraphSegmentForegroundColour' ::
  HasParagraphSegmentStyle a t =>
  Traversal' a (Colour Float)
paragraphSegmentForegroundColour' =
  paragraphSegmentForegroundColour . _Just

paragraphSegmentBackgroundColour' ::
  HasParagraphSegmentStyle a t =>
  Traversal' a (Colour Float)
paragraphSegmentBackgroundColour' =
  paragraphSegmentBackgroundColour . _Just

paragraphSegmentFont' ::
  HasParagraphSegmentStyle a t =>
  Traversal' a t
paragraphSegmentFont' =
  paragraphSegmentFont . _Just

paragraphSegmentFontSize' ::
  HasParagraphSegmentStyle a t =>
  Traversal' a FontSize
paragraphSegmentFontSize' =
  paragraphSegmentFontSize . _Just

class AsParagraphSegmentStyle a t | a -> t where
  _ParagraphSegmentStyle ::
    Prism' a (ParagraphSegmentStyle t)

instance AsParagraphSegmentStyle (ParagraphSegmentStyle s) s where
  _ParagraphSegmentStyle =
    id

class ManyParagraphSegmentStyle a t | a -> t where
  _ParagraphSegmentStyle_ ::
    Traversal' a (ParagraphSegmentStyle t)

instance ManyParagraphSegmentStyle (ParagraphSegmentStyle s) s where
  _ParagraphSegmentStyle_ =
    id

instance ManyFontSize (ParagraphSegmentStyle s) where
  _FontSize_ =
    paragraphSegmentFontSize . _Just
