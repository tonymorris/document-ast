{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE LambdaCase #-}

module Document.AST.HeadingLevel(
  HeadingLevel(..)
, AsHeadingLevel(..)
, HasHeadingLevel(..)
, ManyHeadingLevel(..)
, headingLevelValue
, headingLevelString
) where

import Control.Category((.), id)
import Control.Lens(Prism', Lens', Traversal', prism', review)
import Data.Eq(Eq)
import Data.Int(Int)
import Data.Maybe(Maybe(Nothing, Just))
import Data.Ord(Ord)
import Data.String(String)
import GHC.Show(Show(show))

data HeadingLevel =
  HeadingLevel1
  | HeadingLevel2
  | HeadingLevel3
  | HeadingLevel4
  | HeadingLevel5
  | HeadingLevel6
  deriving (Eq, Ord, Show)

class AsHeadingLevel a where
  _HeadingLevel ::
    Prism' a HeadingLevel
  _HeadingLevel1 ::
    Prism' a ()
  _HeadingLevel1 =
    _HeadingLevel . _HeadingLevel1
  _HeadingLevel2 ::
    Prism' a ()
  _HeadingLevel2 =
    _HeadingLevel . _HeadingLevel2
  _HeadingLevel3 ::
    Prism' a ()
  _HeadingLevel3 =
    _HeadingLevel . _HeadingLevel3
  _HeadingLevel4 ::
    Prism' a ()
  _HeadingLevel4 =
    _HeadingLevel . _HeadingLevel4
  _HeadingLevel5 ::
    Prism' a ()
  _HeadingLevel5 =
    _HeadingLevel . _HeadingLevel5
  _HeadingLevel6 ::
    Prism' a ()
  _HeadingLevel6 =
    _HeadingLevel . _HeadingLevel6

instance AsHeadingLevel HeadingLevel where
  _HeadingLevel =
    id
  _HeadingLevel1 =
    prism'
      (\() -> HeadingLevel1)
      (
        \case
          HeadingLevel1 ->
            Just ()
          _ -> 
            Nothing
      )
  _HeadingLevel2 =
    prism'
      (\() -> HeadingLevel2)
      (
        \case
          HeadingLevel2 ->
            Just ()
          _ -> 
            Nothing
      )
  _HeadingLevel3 =
    prism'
      (\() -> HeadingLevel3)
      (
        \case
          HeadingLevel3 ->
            Just ()
          _ -> 
            Nothing
      )
  _HeadingLevel4 =
    prism'
      (\() -> HeadingLevel4)
      (
        \case
          HeadingLevel4 ->
            Just ()
          _ -> 
            Nothing
      )
  _HeadingLevel5 =
    prism'
      (\() -> HeadingLevel5)
      (
        \case
          HeadingLevel5 ->
            Just ()
          _ -> 
            Nothing
      )
  _HeadingLevel6 =
    prism'
      (\() -> HeadingLevel6)
      (
        \case
          HeadingLevel6 ->
            Just ()
          _ -> 
            Nothing
      )

class HasHeadingLevel a where
  headingLevel ::
    Lens' a HeadingLevel

instance HasHeadingLevel HeadingLevel where
  headingLevel =
    id

class ManyHeadingLevel a where
  _HeadingLevel_ ::
    Traversal' a HeadingLevel

instance ManyHeadingLevel HeadingLevel where
  _HeadingLevel_ =
    id

headingLevelValue ::
  Prism'
    Int
    HeadingLevel
headingLevelValue =
  prism'
    (
      \case
        HeadingLevel1 ->
          1
        HeadingLevel2 ->
          2
        HeadingLevel3 ->
          3
        HeadingLevel4 ->
          4
        HeadingLevel5 ->
          5
        HeadingLevel6 ->
          6        
    )
    (
      \case
        1 ->
          Just HeadingLevel1
        2 ->
          Just HeadingLevel2
        3 ->
          Just HeadingLevel3
        4 ->
          Just HeadingLevel4
        5 ->
          Just HeadingLevel5
        6 ->
          Just HeadingLevel6
        _ ->
          Nothing
    )

headingLevelString ::
  Prism'
    String
    HeadingLevel
headingLevelString =
  prism'
    (show . review headingLevelValue)
    (
      \case
        "1" ->
          Just HeadingLevel1
        "2" ->
          Just HeadingLevel2
        "3" ->
          Just HeadingLevel3
        "4" ->
          Just HeadingLevel4
        "5" ->
          Just HeadingLevel5
        "6" ->
          Just HeadingLevel6
        _ ->
          Nothing
    )
