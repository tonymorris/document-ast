{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}

module Document.AST.LinkStyle(
  LinkStyle(..)
, HasLinkStyle(..)
, AsLinkStyle(..)
, ManyLinkStyle(..)
, linkStyleColour'
, linkStyleDecoration'
, linkStyleOnclick'
, linkStyleOnclick''
) where

import Control.Applicative((<|>))
import Control.Category((.), id)
import Control.Lens(Lens', Prism', Traversal', _Just, over, both)
import Data.Colour(Colour)
import Data.Eq(Eq)
import Data.Functor(Functor(fmap))
import Data.List.NonEmpty(NonEmpty)
import Data.Maybe(Maybe(Nothing))
import Data.Semigroup(Semigroup((<>)))
import Data.Monoid(Monoid(mappend, mempty))
import Data.Traversable(traverse)
import Document.AST.TextInlineDecorations(TextInlineDecorations)
import GHC.Float(Float)
import GHC.Show(Show)

data LinkStyle s =
  LinkStyle
    (Maybe (Colour Float))
    (Maybe (TextInlineDecorations))
    (Maybe (NonEmpty (s, s))) -- onclick
  deriving (Eq, Show)

instance Semigroup (LinkStyle s) where
  LinkStyle c1 d1 oc1 <> LinkStyle c2 d2 oc2 =
    LinkStyle (c1 <|> c2) (d1 <|> d2) (oc1 <|> oc2)
    
instance Monoid (LinkStyle s) where
  mappend =
    (<>)
  mempty =
      LinkStyle
        Nothing
        Nothing
        Nothing

instance Functor LinkStyle where
  fmap f (LinkStyle c d oc) =
    LinkStyle c d (fmap (fmap (over both f)) oc)

class HasLinkStyle a t | a -> t where
  linkStyle ::
    Lens' a (LinkStyle t)
  linkStyleColour ::
    Lens' a (Maybe (Colour Float))
  linkStyleDecoration ::
    Lens' a (Maybe TextInlineDecorations)
  linkStyleOnclick ::
    Lens' a (Maybe (NonEmpty (t, t)))
  linkStyleColour =
    linkStyle . linkStyleColour
  linkStyleDecoration =
    linkStyle . linkStyleDecoration
  linkStyleOnclick =
    linkStyle . linkStyleOnclick

instance HasLinkStyle (LinkStyle s) s where
  linkStyle =
    id
  linkStyleColour f (LinkStyle c d oc) =
    fmap (\c' -> LinkStyle c' d oc) (f c)
  linkStyleDecoration f (LinkStyle c d oc) =
    fmap (\d' -> LinkStyle c d' oc) (f d)
  linkStyleOnclick f (LinkStyle c d oc) =
    fmap (\oc' -> LinkStyle c d oc') (f oc)

linkStyleColour' ::
  HasLinkStyle a t =>
  Traversal' a (Colour Float)
linkStyleColour' =
  linkStyleColour . _Just

linkStyleDecoration' ::
  HasLinkStyle a t =>
  Traversal' a TextInlineDecorations
linkStyleDecoration' =
  linkStyleDecoration . _Just

linkStyleOnclick' ::
  HasLinkStyle a t =>
  Traversal' a (NonEmpty (t, t))
linkStyleOnclick' =
  linkStyleOnclick . _Just

linkStyleOnclick'' ::
  HasLinkStyle a t =>
  Traversal' a t
linkStyleOnclick'' =
  linkStyleOnclick' . traverse . both

class AsLinkStyle a t | a -> t where
  _LinkStyle ::
    Prism' a (LinkStyle t)

instance AsLinkStyle (LinkStyle s) s where
  _LinkStyle =
    id

class ManyLinkStyle a t | a -> t where
  _LinkStyle_ ::
    Traversal' a (LinkStyle t)

instance ManyLinkStyle (LinkStyle s) s where
  _LinkStyle_ =
    id
