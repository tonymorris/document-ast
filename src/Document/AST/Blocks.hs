{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}

module Document.AST.Blocks(
  Blocks(..)
, HasBlocks(..)
, AsBlocks(..)
, ManyBlocks(..)
, StyledBlock(..)
, HasStyledBlock(..)
, AsStyledBlock(..)
, ManyStyledBlock(..)
, Block(..)
, HasBlock(..)
, AsBlock(..)
, ManyBlock(..)
, OrderedList(..)
, HasOrderedList(..)
, AsOrderedList(..)
, ManyOrderedList(..)
, BulletList(..)
, HasBulletList(..)
, AsBulletList(..)
, ManyBulletList(..)
, orderedBulletList
, str
, hr
, ol
, ol'
, ul
, ul'
) where

import Control.Applicative(pure)
import Control.Category((.), id)
import Control.Lens(Prism', Lens', Traversal', Iso, prism', iso, from, over, firsting, seconding, Cons(_Cons), Snoc(_Snoc), AsEmpty(_Empty), Each(each), Reversing(reversing), Plated(plate), Wrapped(Unwrapped, _Wrapped'), _Wrapped, Rewrapped, ( # ))
import Data.Eq(Eq)
import Data.Functor(Functor(fmap), (<$>))
import Data.Maybe(Maybe(Nothing, Just))
import Data.Monoid(Monoid(mempty, mappend))
import Data.Semigroup(Semigroup((<>)))
import Data.Traversable(traverse)
import Document.AST.BlockStyle(BlockStyle, HasBlockStyle(blockStyle), ManyBlockStyle(_BlockStyle_))
import Document.AST.Heading(Heading, AsHeading(_Heading), ManyHeading(_Heading_))
import Document.AST.HorizontalRule(HorizontalRule, AsHorizontalRule(_HorizontalRule))
import Document.AST.Image(Image, AsImage(_Image), ManyImage(_Image_))
import Document.AST.Paragraph(Paragraph, AsParagraph(_Paragraph), ManyParagraph(_Paragraph_))
import Document.AST.TextInline(TextInline(TextInline), _TextInline)
import Document.AST.StyledParagraphSegment(StyledParagraphSegment(StyledParagraphSegment))
import GHC.Show(Show)

newtype Blocks s =
  Blocks [StyledBlock s]
  deriving (Eq, Show)

instance Functor Blocks where
  fmap f (Blocks x) =
    Blocks (fmap (fmap f) x)
    
class HasBlocks s t | s -> t where
  blocks ::
    Lens' s (Blocks t)

instance HasBlocks (Blocks t) t where
  blocks =
    id

class AsBlocks s t | s -> t where
  _Blocks ::
    Prism' s (Blocks t)

instance AsBlocks (Blocks t) t where
  _Blocks =
    id

class ManyBlocks s t | s -> t where
  _Blocks_ ::
    Traversal' s (Blocks t)

instance ManyBlocks (Blocks t) t where
  _Blocks_ =
    id

instance Semigroup (Blocks s) where
  Blocks a <> Blocks b =
    Blocks (a <> b)

instance Monoid (Blocks s) where
  mempty =
    Blocks mempty
  mappend =
    (<>)

instance Cons (Blocks s) (Blocks s) (StyledBlock s) (StyledBlock s) where
  _Cons =
    _Wrapped . _Cons . seconding (from _Wrapped)

instance Snoc (Blocks s) (Blocks s) (StyledBlock s) (StyledBlock s) where
  _Snoc =
    _Wrapped . _Snoc . firsting (from _Wrapped)

instance AsEmpty (Blocks s) where
  _Empty =
    _Wrapped . _Empty

instance Each (Blocks s) (Blocks s) (StyledBlock s) (StyledBlock s) where
  each =
    _Wrapped . each

instance Reversing (Blocks s) where
  reversing =
    over _Wrapped reversing

instance Plated (Blocks s) where
  plate =
    _Wrapped . plate . from _Wrapped

instance Blocks s ~ t =>
  Rewrapped (Blocks s') t

instance Wrapped (Blocks s) where
  type Unwrapped (Blocks s) =
    [StyledBlock s]
  _Wrapped' =
    iso (\(Blocks x) -> x) Blocks

instance ManyBlock (Blocks s) s where
  _Block_ =
    _Wrapped . traverse . _Block_

data Block s =
  ParagraphBlock (Paragraph s)
  | HorizontalRuleBlock HorizontalRule
  | ImageBlock (Image s)
  | HeadingBlock (Heading s)
  | OrderedListBlock (OrderedList s)
  | BulletListBlock (BulletList s)
  deriving (Eq, Show)

instance Functor Block where
  fmap f (ParagraphBlock x) =
    ParagraphBlock (fmap f x)
  fmap _ (HorizontalRuleBlock x) =
    HorizontalRuleBlock x
  fmap f (ImageBlock x) =
    ImageBlock (fmap f x)
  fmap f (HeadingBlock x) =
    HeadingBlock (fmap f x)
  fmap f (OrderedListBlock x) =
    OrderedListBlock (fmap f x)
  fmap f (BulletListBlock x) =
    BulletListBlock (fmap f x)

class AsBlock s t | s -> t where
  _Block ::
    Prism' s (Block t)
  _HorizontalRuleBlock ::
    Prism' s HorizontalRule
  _HorizontalRuleBlock =
    _Block . _HorizontalRuleBlock

instance AsBlock (Block t) t where
  _Block =
    id
  _HorizontalRuleBlock =
    prism'
      HorizontalRuleBlock
      (
        \case
          HorizontalRuleBlock x ->
            Just x
          _ ->
            Nothing
      )

class HasBlock s t | s -> t where
  block ::
    Lens' s (Block t)

instance HasBlock (Block t) t where
  block =
    id

class ManyBlock s t | s -> t where
  _Block_ ::
    Traversal' s (Block t)

instance ManyBlock (Block t) t where
  _Block_ _ (ParagraphBlock x) =
    pure (ParagraphBlock x)
  _Block_ _ (HorizontalRuleBlock x) =
    pure (HorizontalRuleBlock x)
  _Block_ _ (ImageBlock x) =
    pure (ImageBlock x)
  _Block_ _ (HeadingBlock x) =
    pure (HeadingBlock x)
  _Block_ f (OrderedListBlock x) =
    OrderedListBlock <$> _Block_ f x
  _Block_ f (BulletListBlock x) =
    BulletListBlock <$> _Block_ f x

instance AsParagraph (Block s) s where
  _Paragraph =
    prism'
      (\x -> ParagraphBlock x)
      (
        \case
          ParagraphBlock x ->
            Just x
          _ ->
            Nothing
      )

instance AsHorizontalRule (Block s) where
  _HorizontalRule =
    prism'
      (\x -> HorizontalRuleBlock x)
      (
        \case
          HorizontalRuleBlock x ->
            Just x
          _ ->
            Nothing
      )

instance ManyParagraph (Block s) s where
  _Paragraph_ = 
    _Paragraph

instance AsImage (Block s) s where
  _Image =
    prism'
      (\x -> ImageBlock x)
      (
        \case
          ImageBlock x ->
            Just x
          _ ->
            Nothing
      )

instance ManyImage (Block s) s where
  _Image_ = 
    _Image
    
instance AsHeading (Block s) s where
  _Heading =
    prism'
      (\x -> HeadingBlock x)
      (
        \case
          HeadingBlock x ->
            Just x
          _ ->
            Nothing
      )

instance ManyHeading (Block s) s where
  _Heading_ = 
    _Heading
    
instance AsOrderedList (Block s) s where
  _OrderedList =
    prism'
      (\x -> OrderedListBlock x)
      (
        \case
          OrderedListBlock x ->
            Just x
          _ ->
            Nothing
      )

instance ManyOrderedList (Block s) s where
  _OrderedList_ = 
    _OrderedList

instance AsBulletList (Block s) s where
  _BulletList =
    prism'
      (\x -> BulletListBlock x)
      (
        \case
          BulletListBlock x ->
            Just x
          _ ->
            Nothing
      )

instance ManyBulletList (Block s) s where
  _BulletList_ = 
    _BulletList

newtype OrderedList s =
  OrderedList [Blocks s]
  deriving (Eq, Show)

instance Functor OrderedList where
  fmap f (OrderedList x) =
    OrderedList (fmap (fmap f) x)
    
class HasOrderedList s t | s -> t where
  orderedList ::
    Lens' s (OrderedList t)

instance HasOrderedList (OrderedList t) t where
  orderedList =
    id

class AsOrderedList s t | s -> t where
  _OrderedList ::
    Prism' s (OrderedList t)

instance AsOrderedList (OrderedList t) t where
  _OrderedList =
    id

class ManyOrderedList s t | s -> t where
  _OrderedList_ ::
    Traversal' s (OrderedList t)

instance ManyOrderedList (OrderedList t) t where
  _OrderedList_ =
    id

instance Semigroup (OrderedList s) where
  OrderedList a <> OrderedList b =
    OrderedList (a <> b)

instance Monoid (OrderedList s) where
  mempty =
    OrderedList mempty
  mappend =
    (<>)

instance Cons (OrderedList s) (OrderedList s) (Blocks s) (Blocks s) where
  _Cons =
    prism' 
      (\(h, OrderedList t) -> OrderedList (h:t))
      (\(OrderedList t) -> case t of
                        [] -> 
                          Nothing
                        h:t' ->
                          Just (h, OrderedList t'))

instance AsEmpty (OrderedList s) where
  _Empty =
    prism'
      (\() -> OrderedList [])
      (\(OrderedList t) -> case t of
                        [] ->
                          Just ()
                        _:_ -> 
                          Nothing)

instance OrderedList s ~ t =>
  Rewrapped (OrderedList s') t

instance Wrapped (OrderedList s) where
  type Unwrapped (OrderedList s) =
    [Blocks s]
  _Wrapped' =
    iso (\(OrderedList x) -> x) OrderedList

instance ManyBlock (OrderedList s) s where
  _Block_ =
    _Wrapped . traverse . _Block_

data StyledBlock s =
  StyledBlock
    (Block s)
    (BlockStyle s)
  deriving (Eq, Show)

instance Functor StyledBlock where
  fmap f (StyledBlock b s) =
    StyledBlock (fmap f b) (fmap f s)

class HasStyledBlock s t | s -> t where
  styledBlock ::
    Lens' s (StyledBlock t)
    
instance HasStyledBlock (StyledBlock s) s where
  styledBlock =
    id

class AsStyledBlock s t | s -> t where
  _StyledBlock ::
    Prism' s (StyledBlock t)

instance AsStyledBlock (StyledBlock s) s where
  _StyledBlock =
    id
    
class ManyStyledBlock s t | s -> t where
  _StyledBlock_ ::
    Traversal' s (StyledBlock t)
    
instance ManyStyledBlock (StyledBlock s) s where
  _StyledBlock_ =
    id

instance HasBlockStyle (StyledBlock s) s where
  blockStyle f (StyledBlock b s) =
    fmap (\s' -> StyledBlock b s') (f s)

instance ManyBlockStyle (StyledBlock s) s where
  _BlockStyle_ =
    blockStyle

instance HasBlock (StyledBlock s) s where
  block f (StyledBlock b s) =
    fmap (\b' -> StyledBlock b' s) (f b)

instance ManyBlock (StyledBlock s) s where
  _Block_ =
    block

newtype BulletList s =
  BulletList [Blocks s]
  deriving (Eq, Show)

instance Functor BulletList where
  fmap f (BulletList x) =
    BulletList (fmap (fmap f) x)

class HasBulletList s t | s -> t where
  bulletList ::
    Lens' s (BulletList t)

instance HasBulletList (BulletList t) t where
  bulletList =
    id

class AsBulletList s t | s -> t where
  _BulletList ::
    Prism' s (BulletList t)

instance AsBulletList (BulletList t) t where
  _BulletList =
    id

class ManyBulletList s t | s -> t where
  _BulletList_ ::
    Traversal' s (BulletList t)

instance ManyBulletList (BulletList t) t where
  _BulletList_ =
    id

instance Semigroup (BulletList s) where
  BulletList a <> BulletList b =
    BulletList (a <> b)

instance Monoid (BulletList s) where
  mempty =
    BulletList mempty
  mappend =
    (<>)

instance Cons (BulletList s) (BulletList s) (Blocks s) (Blocks s) where
  _Cons =
    prism' 
      (\(h, BulletList t) -> BulletList (h:t))
      (\(BulletList t) -> case t of
                        [] -> 
                          Nothing
                        h:t' ->
                          Just (h, BulletList t'))

instance AsEmpty (BulletList s) where
  _Empty =
    prism'
      (\() -> BulletList [])
      (\(BulletList t) -> case t of
                        [] ->
                          Just ()
                        _:_ -> 
                          Nothing)

instance BulletList s ~ t =>
  Rewrapped (BulletList s') t

instance Wrapped (BulletList s) where
  type Unwrapped (BulletList s) =
    [Blocks s]
  _Wrapped' =
    iso (\(BulletList x) -> x) BulletList

instance ManyBlock (BulletList s) s where
  _Block_ =
    _Wrapped . traverse . _Block_

orderedBulletList ::
  Iso (OrderedList s) (OrderedList s') (BulletList s) (BulletList s')
orderedBulletList =
  iso
    (\(OrderedList x) -> BulletList x)
    (\(BulletList x) -> OrderedList x)

str ::
  (Unwrapped b ~ [x], AsParagraph x a, Rewrapped b b) =>
  a
  -> b
str x =
  _Wrapped # [_Paragraph # (_Wrapped # [StyledParagraphSegment (_TextInline # (TextInline x mempty)) mempty])]

hr ::
  AsBlock x s =>
  HorizontalRule
  -> x
hr x =
  _HorizontalRuleBlock # x

ol' ::
  AsOrderedList x s =>
  [Blocks s]
  -> x
ol' =
  (_OrderedList . _Wrapped #)

ol ::
  AsOrderedList x s =>
  [[StyledBlock s]]
  -> x
ol =
  ol' . fmap Blocks

ul' ::
  AsBulletList x s =>
  [Blocks s]
  -> x
ul' =
  (_BulletList . _Wrapped #)
         
ul ::
  AsBulletList x s =>
  [[StyledBlock s]]
  -> x
ul =
  ul' . fmap Blocks
