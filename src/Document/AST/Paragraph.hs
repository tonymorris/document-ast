{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}

module Document.AST.Paragraph(
  Paragraph(..)
, AsParagraph(..)
, HasParagraph(..)
, ManyParagraph(..)
, p
, ptxt
) where

import Control.Category((.), id)
import Control.Lens(Prism', Lens', Traversal', Rewrapped, Wrapped(_Wrapped'), Unwrapped, _Wrapped, iso, ( # ))
import Data.Eq(Eq)
import Data.Functor(Functor(fmap))
import Data.Monoid(Monoid(mempty, mappend))
import Data.Traversable(traverse)
import Document.AST.Link(ManyLink(_Link_), _Link)
import Document.AST.ParagraphSegment(HasParagraphSegment(paragraphSegment), ManyParagraphSegment(_ParagraphSegment_), txt)
import Document.AST.StyledParagraphSegment(StyledParagraphSegment(StyledParagraphSegment))
import Document.AST.TextInline(ManyTextInline(_TextInline_))
import Document.AST.TextInlineDecoration(ManyTextInlineDecoration(_TextInlineDecoration_))
import Document.AST.TextInlineDecorations(ManyTextInlineDecorations(_TextInlineDecorations_))
import Data.Semigroup(Semigroup((<>)))
import GHC.Show(Show)

newtype Paragraph s =
  Paragraph [StyledParagraphSegment s]
  deriving (Eq, Show)

instance Semigroup s => Semigroup (Paragraph s) where
  Paragraph x <> Paragraph y =
    Paragraph (x <> y)

instance Monoid s => Monoid (Paragraph s) where
  Paragraph x `mappend` Paragraph y =
    Paragraph (x `mappend` y)
  mempty =
    Paragraph mempty
    
instance Functor Paragraph where
  fmap f (Paragraph x) =
    Paragraph (fmap (fmap f) x)

instance Paragraph s ~ t =>
  Rewrapped (Paragraph s') t

instance Wrapped (Paragraph x) where
  type Unwrapped (Paragraph x) =
    [StyledParagraphSegment x]
  _Wrapped' =
    iso
      (\(Paragraph x) -> x)
      Paragraph

class AsParagraph s t | s -> t where
  _Paragraph ::
    Prism' s (Paragraph t)

instance AsParagraph (Paragraph t) t where
  _Paragraph =
    id

class HasParagraph s t | s -> t where
  paragraph ::
    Lens' s (Paragraph t)

instance HasParagraph (Paragraph t) t where
  paragraph =
    id

class ManyParagraph s t | s -> t where
  _Paragraph_ ::
    Traversal' s (Paragraph t)

instance ManyParagraph (Paragraph t) t where
  _Paragraph_ =
    id

instance ManyParagraphSegment (Paragraph s) s where
  _ParagraphSegment_ =
    _Paragraph_ . _ParagraphSegment_

instance ManyTextInline (Paragraph s) s where
  _TextInline_ =
    _Wrapped . traverse . paragraphSegment . _TextInline_

instance ManyLink (Paragraph s) s where
  _Link_ =
    _Wrapped . traverse . paragraphSegment . _Link

instance ManyTextInlineDecorations (Paragraph s) where
  _TextInlineDecorations_ =
    _Wrapped . traverse . paragraphSegment . _TextInlineDecorations_

instance ManyTextInlineDecoration (Paragraph s) where
  _TextInlineDecoration_ =
    _Wrapped . traverse . paragraphSegment . _TextInlineDecoration_

p ::
  AsParagraph x s =>
  [StyledParagraphSegment s] -> x
p =
  (_Paragraph . _Wrapped #)

ptxt ::
  AsParagraph x s =>
  s
  -> x
ptxt t =
  p
    [
      StyledParagraphSegment (txt t) mempty
    ]
