{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE LambdaCase #-}

module Document.AST.FontSize(
  FontSize(..)
, HasFontSize(..)
, AsFontSize(..)
, ManyFontSize(..)
) where

import Control.Category((.), id)
import Control.Lens(Lens', Traversal', Prism', prism')
import Data.Eq(Eq)
import Data.Maybe(Maybe(Just, Nothing))
import Data.Ord(Ord)
import GHC.Show(Show)

data FontSize =
  Medium
  | Small
  | XSmall
  | XXSmall
  | Large
  | XLarge
  | XXLarge
  | Smaller
  | Larger
  deriving (Eq, Ord, Show)

class HasFontSize a where
  fontSize ::
    Lens' a FontSize

instance HasFontSize FontSize where
  fontSize =
    id

class AsFontSize a where
  _FontSize ::
    Prism' a FontSize
  _Small ::
    Prism' a ()
  _XSmall ::
    Prism' a ()
  _XXSmall ::
    Prism' a ()
  _Large ::
    Prism' a ()
  _XLarge ::
    Prism' a ()
  _XXLarge ::
    Prism' a ()
  _Smaller ::
    Prism' a ()
  _Larger ::
    Prism' a ()
  _Small = 
    _FontSize . _Small
  _XSmall = 
    _FontSize . _XSmall
  _XXSmall = 
    _FontSize . _XXSmall
  _Large = 
    _FontSize . _Large
  _XLarge = 
    _FontSize . _XLarge
  _XXLarge = 
    _FontSize . _XXLarge
  _Smaller = 
    _FontSize . _Smaller
  _Larger = 
    _FontSize . _Larger

instance AsFontSize FontSize where
  _FontSize =
    id
  _Small = 
    prism'
      (\() -> Small)
      (
        \case
          Small ->
            Just ()
          _ ->
            Nothing
      )
  _XSmall = 
    prism'
      (\() -> XSmall)
      (
        \case
          XSmall ->
            Just ()
          _ ->
            Nothing
      )
  _XXSmall = 
    prism'
      (\() -> XXSmall)
      (
        \case
          XXSmall ->
            Just ()
          _ ->
            Nothing
      )
  _Large = 
    prism'
      (\() -> Large)
      (
        \case
          Large ->
            Just ()
          _ ->
            Nothing
      )
  _XLarge = 
    prism'
      (\() -> XLarge)
      (
        \case
          XLarge ->
            Just ()
          _ ->
            Nothing
      )
  _XXLarge = 
    prism'
      (\() -> XXLarge)
      (
        \case
          XXLarge ->
            Just ()
          _ ->
            Nothing
      )
  _Smaller = 
    prism'
      (\() -> Smaller)
      (
        \case
          Smaller ->
            Just ()
          _ ->
            Nothing
      )
  _Larger = 
    prism'
      (\() -> Larger)
      (
        \case
          Larger ->
            Just ()
          _ ->
            Nothing
      )

class ManyFontSize a where
  _FontSize_ ::
    Traversal' a FontSize

instance ManyFontSize FontSize where
  _FontSize_ =
    id
