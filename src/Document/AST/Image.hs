{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}

module Document.AST.Image(
  Image(..)
, HasImage(..)
, AsImage(..)
, ManyImage(..)
, imageAltLink
, img
) where

import Control.Applicative((<*>))
import Control.Category((.), id)
import Control.Lens(Prism', Lens', Traversal', Traversal, ( # ))
import Data.Eq(Eq)
import Data.Functor(Functor(fmap), (<$>))
import Data.Monoid(Monoid(mempty, mappend))
import Data.Ord(Ord)
import Data.Semigroup(Semigroup((<>)))
import GHC.Show(Show)

data Image s =
  Image s s
  deriving (Eq, Ord, Show)

instance Semigroup s => Semigroup (Image s) where
  Image a x <> Image b y =
    Image (a <> b) (x <> y)

instance Monoid s => Monoid (Image s) where
  Image a x `mappend` Image b y =
    Image (a `mappend` b) (x `mappend` y)
  mempty =
    Image mempty mempty

instance Functor Image where
  fmap f (Image a l) =
    Image (f a) (f l)
    
class HasImage a s | a -> s where
  image ::
    Lens' a (Image s)
  imagealt ::
    Lens' a s
  imagealt =
    image . imagealt
  {-# INLINE imagealt #-}
  imagelink ::
    Lens' a s
  imagelink =
    image . imagelink
  {-# INLINE imagelink #-}

instance HasImage (Image s) s where
  image =
    id
  {-# INLINE imagealt #-}
  imagealt f (Image a l) =
    fmap (\a' -> Image a' l) (f a)
  {-# INLINE imagelink #-}
  imagelink f (Image a l) =
    fmap (\l' -> Image a l') (f l)

class AsImage a s | a -> s where
  _Image ::
    Prism' a (Image s)

instance AsImage (Image s) s where
  _Image =
    id

class ManyImage a s | a -> s where
  _Image_ ::
    Traversal' a (Image s)

instance ManyImage (Image s) s where
  _Image_ =
    id

imageAltLink ::
  Traversal (Image s) (Image s') s s'
imageAltLink f (Image a l) =
  Image <$> f a <*> f l

img ::
  AsImage x s =>
  s
  -> s
  -> x
img a s =
  _Image # Image a s
