{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}

module Document.AST.TextInlineDecorations(
  TextInlineDecorations(..)
, HasTextInlineDecorations(..)
, AsTextInlineDecorations(..)
, ManyTextInlineDecorations(..)
, oneDecoration
, addDecoration
, (!@~)
) where

import Control.Category((.), id)
import Control.Lens(Prism', Lens', Traversal', Rewrapped, Wrapped(_Wrapped', Unwrapped), Cons(_Cons), Snoc(_Snoc), AsEmpty(_Empty), Each(each), Reversing(reversing), Plated(plate), iso, firsting, seconding, from, over, cons, _Wrapped)
import Data.Eq(Eq)
import Data.Ord(Ord)
import Data.Monoid(Monoid(mappend, mempty))
import Data.Semigroup(Semigroup((<>)))
import Data.Traversable(traverse)
import Document.AST.TextInlineDecoration(TextInlineDecoration, ManyTextInlineDecoration(_TextInlineDecoration_))
import GHC.Show(Show)

newtype TextInlineDecorations =
  TextInlineDecorations [TextInlineDecoration]
  deriving (Eq, Ord, Show)

instance Semigroup TextInlineDecorations where
  TextInlineDecorations x <> TextInlineDecorations y =
    TextInlineDecorations (x <> y)

instance Monoid TextInlineDecorations where
  mempty =
    TextInlineDecorations mempty
  mappend =
    (<>)

instance TextInlineDecorations ~ t =>
  Rewrapped TextInlineDecorations t

instance Wrapped TextInlineDecorations where
  type Unwrapped TextInlineDecorations =
    [TextInlineDecoration]
  _Wrapped' =
    iso
      (\(TextInlineDecorations x) -> x)
      TextInlineDecorations

class HasTextInlineDecorations a where
  textInlineDecorations ::
    Lens' a TextInlineDecorations

instance HasTextInlineDecorations TextInlineDecorations where
  textInlineDecorations =
    id

class AsTextInlineDecorations a where
  _TextInlineDecorations ::
    Prism' a TextInlineDecorations

instance AsTextInlineDecorations TextInlineDecorations where
  _TextInlineDecorations =
    id

class ManyTextInlineDecorations a where
  _TextInlineDecorations_ ::
    Traversal' a TextInlineDecorations

instance ManyTextInlineDecorations TextInlineDecorations where
  _TextInlineDecorations_ =
    id

instance Cons TextInlineDecorations TextInlineDecorations TextInlineDecoration TextInlineDecoration where
  _Cons =
    _Wrapped . _Cons . seconding (from _Wrapped)

instance Snoc TextInlineDecorations TextInlineDecorations TextInlineDecoration TextInlineDecoration where
  _Snoc =
    _Wrapped . _Snoc . firsting (from _Wrapped)

instance AsEmpty TextInlineDecorations where
  _Empty =
    _Wrapped . _Empty

instance Each TextInlineDecorations TextInlineDecorations TextInlineDecoration TextInlineDecoration where
  each =
    _Wrapped . each

instance Reversing TextInlineDecorations where
  reversing =
    over _Wrapped reversing

instance Plated TextInlineDecorations where
  plate =
    _Wrapped . plate . from _Wrapped

oneDecoration ::
  TextInlineDecoration
  -> TextInlineDecorations
oneDecoration a =
  TextInlineDecorations [a]

instance ManyTextInlineDecoration TextInlineDecorations where
  _TextInlineDecoration_ =
    _Wrapped . traverse

addDecoration ::
  ManyTextInlineDecorations x =>
  TextInlineDecoration
  -> x
  -> x
addDecoration d x =
  over _TextInlineDecorations_ (cons d) x

(!@~) ::
  ManyTextInlineDecorations x =>
  TextInlineDecoration
  -> x
  -> x
(!@~) =
  addDecoration

infixr 5 !@~
